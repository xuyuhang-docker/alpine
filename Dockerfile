FROM alpine:3.14.1
WORKDIR /root
COPY files/repositories /etc/apk/repositories
COPY files/glibc-2.33-r0.apk /root/glibc.apk
COPY files/glibc-bin-2.33-r0.apk /root/glibc-bin.apk
COPY files/sgerrand.rsa.pub /etc/apk/keys/sgerrand.rsa.pub

# Install glibc
RUN apk add glibc-bin.apk glibc.apk
RUN /usr/glibc-compat/sbin/ldconfig /lib /usr/glibc-compat/lib
RUN echo 'hosts: files mdns4_minimal [NOTFOUND=return] dns mdns4' >> /etc/nsswitch.conf
RUN rm -rf glibc.apk glibc-bin.apk

RUN apk add --update ca-certificates tzdata && \
    rm -rf /var/cache/apk/*
